## Tech Chat - #12

-- performance Testing -- 

![wave](Images/logo1.png)

---

##### Performance Testing and Load Testing Basics 

1. Introduction to Performance Testing 
2. Difference between performance, Load and Stress testing 
3. Why Performance testing 
4. When is it required?
5. What should be tested?
6. Performance testing Process
7. Load test configuration for a web system
8. Practice Question 

--- 

#### Introduction to Performance Testing 

Performance testing is the process of determine the speed or effectiveness of a computer, network, software programme or device

Before going into the details, we should understand the factors that governs performance testing
  * Throughput
  
  * Response time
  
  * Tunining
  
  * Benchmarking 
  
  --- 
  
#### Throughput 

![Throughput Curve](images/throughput-curve.png)


+++
#### Throughput 

- Capability of a product to handle multiple transactions in a given period 

- Throughput represent the number of request/business transactions processed by in a specified time duration

- As the number of concurrent users increase, the throughput increase almost linearly with the number of request. As there is very little congestion within the APP 


+++
#### Throughput 

![Throughput Curve] (images/throughput-curve.png)


+++
#### Throughput 

- In the heavy load zone or section B, as the concurrent client load increases, throughput remains relatively constnat

- In section C (the buckle zone) one or more of the system components have become exhausted and throughput starts to degrade. For example, the system might enter the


---
#### Response Time 

![Response Time](images/responsetime.png)


---
#### Tuning 

![Tuning](images/tuning.png)


+++
#### Tuning 

* Tuning is the procedureby which product performance is  enhanced by setting different values to the parameters of the product.

* Tuning improves the  product perfromance without having to touch the source code of the product. 


---
#### Benchmarking 

![Benchmarking](images/benchmarking.png)


+++
#### Benchmarking 

* A Very well-improved performance of a product makes no business sense if that performance does not match up to competitive products

* A careful analysis is needed to chalk out the list of transaction to be compared across product so that an apple-apple cpmparison becomes possible


---
#### Performance Testing - Definition 

* The testing to evaluate the response time (speed), throughput and utilization of system to execute its required functions in comparison with different versions of the same product or a different competitive product is called Performance Testing.

* Performance testing is done to derive benchmark numbers for the system.


---
#### Performance Testing - Definition 

* Heavy load is not applied to the system

* Tuning is performed until the system under test achieves the expected levels of performance


---
#### Difference between performance, load and stress testing

+++
#### Load testing 

* Process of exercising the system under test by feeding it the largest takes it can operate with.

* Constantly increasing the load on the system via automated tools to simulate real time scenario with virtual users


+++
#### Load testing 

![Load test](image/loadtest.png)


+++
#### Examples:  

* For web help application load is defined in terms of concurrent users ot HTTP connections.


---
#### Stress testing 

* Trying to break the system under test by overwhelming its resources or by taking resources away from it.

* Purpose is to make sure that the system fails and recovers gracefully.


+++
#### Stress testing 

![Stress Terst](images/stresstest.png)


+++
#### Examples:

Double the baseline number for concurrent users/HTTP connections.

* Double the baseline number for concurrent users/HTTP connections.

* Randomly shut down and restart ports on the network switches/routers that connects servers. 

---
#### Why performance testing?

The week of Feb 6, 2000: A young hacker delivered over 1-Billion transactions concurrently to each one of these sites.

* Yahoo
* Ebay
* Buy.ocm
* Amazon
* E trade 

How did you think they performed? 

(Read: https://goo.gl/4bkofY)


+++
#### Why performance testing? 

* dentifies problems early on before they become costly to resolve.

* Reduces development cycles.

* Produces better quality, more scalable code.


+++
#### Why Performance testing

* Prevents revenue and credibility loss due to poor Web site performance.

* Enables intelligent planning for future expansion. 

* To ensure that the system meets performance expectations such as response time, throughput etc. under given levels of load.
  Expose bugs that do not surface in cursory testing, such as memory management bugs, memory leaks, buffer overflows, etc.


---
#### When is it required? 

* Design phase
    * Pages containing lots of images and multimedia for reasonable wait times. Heavy loads are less important than knowing which types of content cause slowdowns 

* Development phase 
    * To check results of individual pages and processes, looking for breaking points, unnecessary code and bottlenecks.


+++
#### When is it required? 

*  To identify the minimum hardware and software requirements for the application.


---
#### What should be tested?

* Read Transactions: At least one READ ONLY transaction should be included, so that performance of such transactions can be differentiated from other more complex transactions.

* Update Transactions: At least one update transaction should be included so that performance of such transactions can be differentiated from other transactions.


---
#### Propermance testing process

![Performance test process](images/performancetestingprocess.png)


---
#### 1. Planing >

* Determine the performance testing objectives

* Describe the application to test using a aplication model.

   * Describe the hardware environment 

   * Create a Benchmark (Agenda) to be recorded in Phase 2.
   
     * Define what tasks each user will perform.
    
     * Define (or estimate) the percentage of users per task.


---
#### 1. Planning  2. Record 


* Record 
  * Record the defined testing activities that will be used as a foundation for your load test scripts.
  * One activity per task or multiple activities depending on user task definition. 
  

---
#### 1. Planing  2. Record  3. Modify 

* Modify 
  * Modify load test scripts defined by recorder to reflect more realistic Load test simulations.
  * Defining the project, users.
  * Randomize parameters (Data, times, environment).
  * andomize user activities that occur during the load test. 
  

---

























 




  
  



